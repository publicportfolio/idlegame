# Idle Game

방치형 게임

### 개발 환경

- C#
- Unity 2022.3.22f1
- JavaScript
- Node.js
- MongoDB

### 주요 기능

- 엑셀 템플릿을 JSON으로 변환하는 유니티 에디터 구현
- 모바일 해상도에 따른 UI 대응
- 각종 매니저로 코드 전반 관리
- 클라 서버 HTTP 통신 기능 구현
- Amazon EC2로 게임 서버 구축
- 사용자 데이터 MongoDB에 저장

### 다운로드

- [주요 코드 설명](https://docs.google.com/presentation/d/1EMKPB8PHCjGBskZyZRGgyAEdx-Kk_gvA/edit?usp=drive_link&ouid=116539910755259840488&rtpof=true&sd=true)
- [윈도우버전 빌드](https://drive.google.com/file/d/1zOE-sCal-4-H13funlnfUZRzBv6pd5AG/view?usp=drive_link)
- [안드로이드버전 빌드](https://drive.google.com/file/d/1xcKuXP__IyZ9L6ErYWvY0gaXO0htPNMC/view?usp=drive_link)
