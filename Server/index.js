const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const { UserData } = require("./userData");
const dotenv = require("dotenv");

dotenv.config();

const run = async () => {
  const url = process.env.MONGO_URL;
  await mongoose.connect(url, {
    dbName: "gameData",
  });

  const server = express();
  const port = 3000;

  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({ extended: true }));

  server.post("", async (req, res) => {
    let protocol = req.body.protocol;

    const userData = new UserData();
    let resJson;

    switch (protocol) {
      case 100000: //로그인
        resJson = await userData.login(req.body);
        break;
      case 100010: //스테이지 종료
        resJson = await userData.stageEnd(req.body);
        break;
      case 100020: //스텟 구매
        resJson = await userData.buyStat(req.body);
        break;
      case 100030: //아이템 구매
        resJson = await userData.buyItem(req.body);
        break;
      case 100040: //적 처치
        resJson = await userData.enemyDead(req.body);
        break;
    }

    resJson = {
      protocol: req.body.protocol + 1,
      data: resJson,
      uuid: req.body.uuid,
    };

    res.json(resJson);
  });

  server.listen(port, () => {
    console.log(`Server is running on port ${port}`);
  });
};

run();
