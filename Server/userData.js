const enemyTemplate = require("./src/template/EnemyTemplate");
const itemTemplate = require("./src/template/ItemTemplate");
const statTemplate = require("./src/template/StatTemplate");
const { User } = require("./src/models/user.model");

class UserData {
  findTemplate(id, templates) {
    const exist = templates.find((template) => {
      return id === template.id;
    });

    return exist;
  }

  async login(body) {
    const userFind = await User.findOne({ uuid: body.uuid });
    let gameData;

    if (!userFind) {
      gameData = {
        uuid: body.uuid,
        gold: 50000,
        attLv: 1,
        attSpeedLv: 1,
        criRateLv: 1,
        criDamageLv: 1,
        attPotionDate: 0.0,
        criRatePotionDate: 0.0,

        stageType: 1,
        stageKind: 1,

        isBgm: true,
        isEffect: true,
        language: 0,
      };

      await User.create(gameData);

      delete gameData.uuid;
    } else {
      const { _id, uuid, __v, id, createdAt, updatedAt, ...data } =
        userFind.toObject();
      gameData = data;
    }

    return gameData;
  }

  async stageEnd(body) {
    await User.updateOne({ uuid: body.uuid }, body.data);

    return body.data;
  }

  async buyStat(body) {
    const template = this.findTemplate(
      body.id,
      JSON.parse(JSON.stringify(statTemplate)).statTemplates
    );

    const userFind = await User.findOne({ uuid: body.uuid });
    userFind.gold -= template.price;

    if (template.type === 1) userFind.attLv = template.level;
    else if (template.type === 2) userFind.attSpeedLv = template.level;
    else if (template.type === 3) userFind.criRateLv = template.level;
    else if (template.type === 4) userFind.criDamageLv = template.level;

    let gameData = {
      gold: userFind.gold,
      attLv: userFind.attLv,
      attSpeedLv: userFind.attSpeedLv,
      criRateLv: userFind.criRateLv,
      criDamageLv: userFind.criDamageLv,
    };

    await User.updateOne({ uuid: body.uuid }, gameData);

    return gameData;
  }

  async buyItem(body) {
    const template = this.findTemplate(
      body.id,
      JSON.parse(JSON.stringify(itemTemplate)).itemTemplates
    );

    const userFind = await User.findOne({ uuid: body.uuid });
    userFind.gold -= template.price;

    let date = new Date();
    date.setMinutes(date.getMinutes() + template.time);

    if (template.type === 1) userFind.attPotionDate = date.getTime();
    else if (template.type === 2) userFind.criRatePotionDate = date.getTime();

    let gameData = {
      gold: userFind.gold,
      attPotionDate: userFind.attPotionDate,
      criRatePotionDate: userFind.criRatePotionDate,
    };

    await User.updateOne({ uuid: body.uuid }, gameData);

    return gameData;
  }

  async enemyDead(body) {
    const template = this.findTemplate(
      body.id,
      JSON.parse(JSON.stringify(enemyTemplate)).enemyTemplates
    );

    const userFind = await User.findOne({ uuid: body.uuid });
    userFind.gold += template.gold;

    let gameData = {
      gold: userFind.gold,
    };

    await User.updateOne({ uuid: body.uuid }, gameData);

    return gameData;
  }
}

module.exports = { UserData };
