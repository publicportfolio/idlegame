const { UserSchema } = require("../entities/user");
const mongoose = require("mongoose");

const User = mongoose.model("User", UserSchema, "gameDatas");

module.exports = { User };
