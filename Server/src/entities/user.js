const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
  {
    uuid: { type: String, require: true },
    gold: { type: Number, require: true },
    attLv: { type: Number, require: true },
    attSpeedLv: { type: Number, require: true },
    criRateLv: { type: Number, require: true },
    criDamageLv: { type: Number, require: true },
    attPotionDate: { type: Number, require: true },
    criRatePotionDate: { type: Number, require: true },

    stageType: { type: Number, require: true },
    stageKind: { type: Number, require: true },

    isBgm: { type: Boolean, require: true },
    isEffect: { type: Boolean, require: true },
    language: { type: Number, require: true },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
    timestamps: true,
  }
);

module.exports = { UserSchema };
