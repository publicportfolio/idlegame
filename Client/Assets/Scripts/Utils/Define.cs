using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Define
{
    public enum UIEvent
    {
        Click,
        Press,
        Down,
        Up,
    }

    public enum Scene
    {
        None,
        Title,
        Game,
    }

    public enum Sound
    {
        Bgm,
        Effect,
        Max,
    }

    public enum Language
    {
        None,
        Eng,
        Kor,
    }

    public enum Stat
    {
        Att,
        AttSpeed,
        CriRate,
        CriDamage,
        Max,
    }

    public enum Enemy
    {
        Normal = 1,
        MiddleBoss = 10,
        Boss = 20,
    }

    public enum Item
    {
        None,
        AttPotion,
        CriRatePotion,
        Max,
    }

    public enum BottomItem
    {
        None,
        Stat,
        Shop,
        Max,
    }

    public enum PlayerInfo
    {
        Att,
        AttSpeed,
        CriRate,
        CriDamage,
        AttPotion,
        CriRatePotion,
        Max,
    }

    public enum State
    {
        None,
        Idle,
        Move,
        Hurt,
        Dead,
        Attack1,
        Attack2,
        Attack3,
    }

    public enum Protocol
    {
        REQ_LOGIN = 100000,
        RES_LOGIN,
        REQ_STAGE_END = 100010,
        RES_STAGE_END,
        REQ_BUY_STAT = 100020,
        RES_BUY_STAT,
        REQ_BUY_ITEM = 100030,
        RES_BUY_ITEM,
        REQ_ENEMY_DEAD = 100040,
        RES_ENEMY_DEAD,
    }
}
