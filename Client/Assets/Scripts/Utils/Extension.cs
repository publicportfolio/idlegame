using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public static class Extension
{
    public static void BindEvent(this GameObject go, Action action, Define.UIEvent type = Define.UIEvent.Click)
    {
        UIBase.BindEvent(go, action, type);
    }

    public static void SetSprite(this Image image, string spriteName)
    {
        image.sprite = Managers.Resource.Load<Sprite>($"Textures/UI/{spriteName}");
    }

    public static void SetSprite(this Button button, string spriteName)
    {
        button.GetComponent<Image>().sprite = Managers.Resource.Load<Sprite>($"Textures/UI/{spriteName}");
    }

    public static void SetWidth(this TextMeshProUGUI text, float width)
    {
        RectTransform rect = text.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(width, rect.sizeDelta.y);
    }
}
