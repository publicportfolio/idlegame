using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Util
{
    public static T GetOrAddComponent<T>(GameObject go) where T : Component
    {
        T comp = go.GetComponent<T>();
        if (comp == null)
            comp = go.AddComponent<T>();

        return comp;
    }

    public static T FindChild<T>(GameObject go, string name = null, bool isRecursive = false) where T : Object
    {
        if (go == null)
            return null;

        if (isRecursive == false)
        {
            Transform transform = go.transform.Find(name);
            if (transform != null)
                return transform.GetComponent<T>();
        }
        else
        {
            foreach (T comp in go.GetComponentsInChildren<T>())
            {
                if (string.IsNullOrEmpty(name) || comp.name == name)
                    return comp;
            }
        }

        return null;
    }

    public static GameObject FindChild(GameObject go, string name = null, bool isRecursive = false)
    {
        Transform transform = FindChild<Transform>(go, name, isRecursive);
        if (transform == null)
            return null;

        return transform.gameObject;
    }
}
