using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class UIBase : MonoBehaviour
{
    protected bool m_isInit = false;
    protected int m_type;

    protected List<List<UIBase>> m_subItems = new List<List<UIBase>>();
    protected Dictionary<Type, UnityEngine.Object[]> m_objects = new Dictionary<Type, UnityEngine.Object[]>();

    public int Type { set { m_type = value; } }

    void Start()
    {
        Init();
    }

    protected virtual bool Init()
    {
        if (m_isInit)
            return false;

        return m_isInit = true;
    }

    public virtual void RefreshUI()
    {

    }

    protected void RefreshUISubItems()
    {
        foreach (List<UIBase> list in m_subItems)
        {
            foreach (UIBase item in list)
            {
                if (item != null)
                    item.RefreshUI();
            }
        }
    }

    protected void Bind<T>(Type type) where T : UnityEngine.Object
    {
        string[] names = Enum.GetNames(type);
        UnityEngine.Object[] objects = new UnityEngine.Object[names.Length];
        m_objects.Add(typeof(T), objects);

        for (int i = 0; i < names.Length; i++)
        {
            if (typeof(T) == typeof(GameObject))
                objects[i] = Util.FindChild(gameObject, names[i], true);
            else
                objects[i] = Util.FindChild<T>(gameObject, names[i], true);

            if (objects[i] == null)
                Debug.Log($"[UIBase:Bind] Failed to bind({names[i]})");
        }
    }

    protected void BindObject(Type type)
    {
        Bind<GameObject>(type);
    }

    protected void BindText(Type type)
    {
        Bind<TextMeshProUGUI>(type);
    }

    protected void BindImage(Type type)
    {
        Bind<Image>(type);
    }

    protected void BindButton(Type type)
    {
        Bind<Button>(type);
    }

    protected void BindToggle(Type type)
    {
        Bind<Toggle>(type);
    }

    protected T Get<T>(int idx) where T : UnityEngine.Object
    {
        UnityEngine.Object[] objects = null;
        if (m_objects.TryGetValue(typeof(T), out objects) == false)
            return null;

        return objects[idx] as T;
    }

    protected GameObject GetObject(int idx)
    {
        return Get<GameObject>(idx);
    }

    protected TextMeshProUGUI GetText(int idx)
    {
        return Get<TextMeshProUGUI>(idx);
    }

    protected Image GetImage(int idx)
    {
        return Get<Image>(idx);
    }

    protected Button GetButton(int idx)
    {
        return Get<Button>(idx);
    }

    protected Toggle GetToggle(int idx)
    {
        return Get<Toggle>(idx);
    }

    public static void BindEvent(GameObject go, Action action, Define.UIEvent type = Define.UIEvent.Click)
    {
        UIEventHandler evt = Util.GetOrAddComponent<UIEventHandler>(go);

        switch (type)
        {
            case Define.UIEvent.Click:
            evt.OnClickHandler -= action;
            evt.OnClickHandler += action;
            break;
            case Define.UIEvent.Press:
            evt.OnPressedHandler -= action;
            evt.OnPressedHandler += action;
            break;
            case Define.UIEvent.Down:
            evt.OnPointerDownHandler -= action;
            evt.OnPointerDownHandler += action;
            break;
            case Define.UIEvent.Up:
            evt.OnPointerUpHandler -= action;
            evt.OnPointerUpHandler += action;
            break;
        }
    }

    protected float CalcTextWidth(GameObject parent, float offset, params GameObject[] arr)
    {
        float width = gameObject.GetComponent<RectTransform>().rect.width - offset;
        foreach (GameObject obj in arr)
            width -= obj.GetComponent<RectTransform>().rect.width;

        return width;
    }
}
