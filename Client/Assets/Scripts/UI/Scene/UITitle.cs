using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UITitle : UIScene
{
    enum Texts
    {
        TitleText,
        PressText,
    }

    bool m_isReady = false;
    float m_time = 0.0f;

    TextMeshProUGUI m_pressText;

    protected override bool Init()
    {
        if (base.Init() == false)
            return false;

        BindText(typeof(Texts));

        GetText((int)Texts.TitleText).text = Managers.GetText(1000000001);
        m_pressText = GetText((int)Texts.PressText);
        m_pressText.text = Managers.GetText(1000000002);
        m_pressText.gameObject.SetActive(false);

        return true;
    }

    public override void RefreshUI()
    {
        if (m_isInit == false)
            return;

        m_isReady = true;
        m_pressText.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (m_isReady == false)
            return;

        if (m_time < 1.0f)
        {
            m_pressText.alpha = 1.0f - m_time;
        }
        else
        {
            m_pressText.alpha = m_time - 1.0f;

            if (m_time > 2.0f)
                m_time = 0.0f;
        }

        m_time += Time.deltaTime;

        if (Input.GetMouseButtonDown(0))
            Managers.Scene.LoadScene(Define.Scene.Game);
    }
}
