using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using static Define;

public class UIGame : UIScene
{
    enum GameObjects
    {
        PotionPanel,
        HpBar,
        StatPanel,
        ShopPanel,
        BottomPanel,
    }

    enum Texts
    {
        StageText,
        TimeText,
        GoldText,
        HpBarText,
    }

    enum Buttons
    {
        PlayerInfoButton,
        SettingButton,
    }

    Define.BottomItem m_prevBottom;
    Define.BottomItem m_curBottom;

    GameObject m_hpBar;
    TextMeshProUGUI m_hpBarText;
    TextMeshProUGUI m_timeText;

    public Define.BottomItem CurBottom { set { m_curBottom = value; } }

    protected override bool Init()
    {
        if (base.Init() == false)
            return false;

        m_prevBottom = Define.BottomItem.None;
        m_curBottom = Define.BottomItem.Stat;

        BindObject(typeof(GameObjects));
        BindText(typeof(Texts));
        BindButton(typeof(Buttons));

        m_hpBar = GetObject((int)GameObjects.HpBar);
        GameObject statPanel = GetObject((int)GameObjects.StatPanel);
        GameObject shopPanel = GetObject((int)GameObjects.ShopPanel);
        GameObject buttonPanel = GetObject((int)GameObjects.BottomPanel);

        m_hpBarText = GetText((int)Texts.HpBarText);
        m_timeText = GetText((int)Texts.TimeText);

        Button playerInfo = GetButton((int)Buttons.PlayerInfoButton);
        Button setting = GetButton((int)Buttons.SettingButton);
        playerInfo.gameObject.BindEvent(OnPlayerInfo);
        setting.gameObject.BindEvent(OnSetting);
        playerInfo.SetSprite("PlayerInfo");
        setting.SetSprite("Setting");

        m_subItems.Add(Managers.UI.MakePanelSubItem<UIStatItem>(statPanel, (int)Define.Stat.Max));
        m_subItems.Add(Managers.UI.MakePanelSubItem<UIShopItem>(shopPanel, (int)Define.Item.Max, false));
        m_subItems.Add(Managers.UI.MakePanelSubItem<UIBottomItem>(buttonPanel, (int)Define.BottomItem.Max, false));

        for (int i = 0; i < (int)Define.Item.Max - 1; i++)
        {
            if (Managers.Data.GetPotionSec((Define.Item)i + 1) > 0.0f)
                MakePotionItem((Define.Item)i + 1);
        }

        Canvas.ForceUpdateCanvases();

        RefreshUI();

        return true;
    }

    private void Update()
    {
        float time = Managers.Stage.PlayTime;
        if (time < 10.0f)
        {
            if (m_timeText.color != Color.red)
                m_timeText.color = Color.red;
        }
        else
        {
            if (m_timeText.color != Color.white)
                m_timeText.color = Color.white;
        }

        m_timeText.text = time.ToString("F1");

        EnemyController enemy = Managers.Game.Enemy;
        if (enemy != null)
        {
            m_hpBar.GetComponent<Slider>().value = enemy.Hp / (float)enemy.MaxHp;
            m_hpBarText.text = $"{enemy.Hp.ToString("N0")} / {enemy.MaxHp.ToString("N0")}";
        }
    }

    public override void RefreshUI()
    {
        if (m_isInit == false)
            return;

        if (m_prevBottom != m_curBottom)
        {
            m_prevBottom = m_curBottom;

            bool isStat = m_curBottom == Define.BottomItem.Stat;
            GetObject((int)GameObjects.StatPanel).SetActive(isStat);
            GetObject((int)GameObjects.ShopPanel).SetActive(!isStat);
        }

        GetText((int)Texts.StageText).text = $"{Managers.Stage.GetStageName()} {Managers.Stage.Kind}";
        GetText((int)Texts.GoldText).text = $"{Managers.Data.SaveData.gold.ToString("N0")}G";

        RefreshUISubItems();
    }

    public void MakePotionItem(Define.Item type)
    {
        Transform parent = GetObject((int)GameObjects.PotionPanel).transform;
        Managers.UI.MakeSubItem<UIPotionItem>(parent).Type = (int)type;
    }

    void OnPlayerInfo()
    {
        Debug.Log("[UIGame:OnPlayerInfo] Click");

        Managers.UI.ShowUIPopup<UIPlayerInfo>();
        Managers.Sound.Play(Sound.Effect, "Click");
    }

    void OnSetting()
    {
        Debug.Log("[UIGame:OnSetting] Click");

        Managers.UI.ShowUIPopup<UISetting>();
        Managers.Sound.Play(Sound.Effect, "Click");
    }
}
