using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Define;

public class UIBottomItem : UIBase
{
    enum Texts
    {
        NameText,
    }

    protected override bool Init()
    {
        if (base.Init() == false)
            return false;

        BindText(typeof(Texts));

        gameObject.BindEvent(OnButtonMenu);

        RefreshUI();

        return true;
    }

    public override void RefreshUI()
    {
        if (m_isInit == false)
            return;

        GetText((int)Texts.NameText).text = Managers.GetText(1000000003 + m_type - 1);
    }

    void OnButtonMenu()
    {
        Debug.Log("[UIBottomItem:OnButtonMenu] Click");

        Managers.UI.FindUIScene<UIGame>().CurBottom = (Define.BottomItem)m_type;
        Managers.UI.RefreshUI();
        Managers.Sound.Play(Sound.Effect, "Click");
    }
}
