using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class UIPotionItem : UIBase
{
    enum Images
    {
        PotionFillImage,
    }

    Image m_fillImage;

    protected override bool Init()
    {
        if (base.Init() == false)
            return false;

        BindImage(typeof(Images));

        string iconName = Enum.GetName(typeof(Define.Item), (Define.Item)m_type);
        gameObject.GetComponent<Image>().SetSprite(iconName);
        m_fillImage = GetImage((int)Images.PotionFillImage);
        m_fillImage.SetSprite(iconName);

        return true;
    }

    private void Update()
    {
        float remainSec = (float)Managers.Data.GetPotionSec(m_type);
        float totalSec = Managers.Template.GetItemTemplate(m_type).time * 60.0f;
        float ratio = remainSec / totalSec;

        if (ratio > 0.0f)
            m_fillImage.fillAmount = ratio;
        else
            Managers.Resource.Destroy(gameObject);
    }
}
