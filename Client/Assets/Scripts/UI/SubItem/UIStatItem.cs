using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer.Internal;
using UnityEngine;
using UnityEngine.UI;
using static Define;

public class UIStatItem : UIBase
{
    enum Texts
    {
        PriceText,
        UpgradeText,
        MaxText,
        NameText,
        DetailText,
    }

    enum Images
    {
        IconImage,
    }

    enum Buttons
    {
        UpgradeButton,
    }

    protected override bool Init()
    {
        if (base.Init() == false)
            return false;

        BindText(typeof(Texts));
        BindImage(typeof(Images));
        BindButton(typeof(Buttons));

        Image iconImage = GetImage((int)Images.IconImage);
        iconImage.SetSprite(Enum.GetName(typeof(Define.Stat), (Define.Stat)m_type));

        Button upgradeButton = GetButton((int)Buttons.UpgradeButton);
        upgradeButton.gameObject.BindEvent(OnUpgrade);

        float width = CalcTextWidth(gameObject, 100.0f, iconImage.gameObject, upgradeButton.gameObject);
        GetText((int)Texts.NameText).SetWidth(width);
        GetText((int)Texts.DetailText).SetWidth(width);

        RefreshUI();

        return true;
    }

    public override void RefreshUI()
    {
        if (m_isInit == false)
            return;

        int level = Managers.Data.GetLevel(m_type);
        StatTemplate curTemplate = Managers.Template.GetStatTemplate(m_type, level);
        StatTemplate nextTemplate = Managers.Template.GetStatTemplate(m_type, level + 1);

        string price = "";
        string upgrade = "";
        string detail = "";
        string format = "N0";

        if (m_type == (int)Define.Stat.AttSpeed)
            format = "F2";
        else if (m_type == (int)Define.Stat.CriDamage)
            format = "F3";

        bool isNext = nextTemplate != null;
        if (isNext)
        {
            price = $"{nextTemplate.price.ToString("N0")}G";
            upgrade = Managers.GetText(1000000005);
            detail = $"{curTemplate.value.ToString(format)} -> Lv.{level + 1} {nextTemplate.value.ToString(format)}";
        }
        else
        {
            detail = $"{curTemplate.value.ToString(format)}";
        }

        GetText((int)Texts.PriceText).gameObject.SetActive(isNext);
        GetText((int)Texts.UpgradeText).gameObject.SetActive(isNext);
        GetText((int)Texts.MaxText).gameObject.SetActive(!isNext);
        GetButton((int)Buttons.UpgradeButton).interactable = isNext;

        GetText((int)Texts.PriceText).text = price;
        GetText((int)Texts.UpgradeText).text = upgrade;
        GetText((int)Texts.MaxText).text = Managers.GetText(1000000013);
        GetText((int)Texts.NameText).text = $"Lv.{level} {Managers.GetText(1000000006 + m_type)}";
        GetText((int)Texts.DetailText).text = detail;
    }

    void OnUpgrade()
    {
        Debug.Log("[UIStatItem:OnUpgrade] Click");

        int level = Managers.Data.GetLevel(m_type);
        StatTemplate nextTemplate = Managers.Template.GetStatTemplate(m_type, level + 1);
        if (nextTemplate == null)
            return;

        if (Managers.Data.IsEnoughGold(nextTemplate.price) == false)
            return;

        NetworkManager.Instance.RequestBuyStat(nextTemplate.id);
        Managers.Sound.Play(Sound.Effect, "Buy");
    }
}
