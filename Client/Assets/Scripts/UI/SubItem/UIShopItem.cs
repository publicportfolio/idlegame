using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Define;

public class UIShopItem : UIBase
{
    enum Texts
    {
        PriceText,
        BuyText,
        TimeText,
        NameText,
        DetailText,
    }

    enum Images
    {
        IconImage,
    }

    enum Buttons
    {
        BuyButton,
    }

    ItemTemplate m_template;

    TextMeshProUGUI m_timeText;

    protected override bool Init()
    {
        if (base.Init() == false)
            return false;

        m_template = Managers.Template.GetItemTemplate(m_type);

        BindText(typeof(Texts));
        BindImage(typeof(Images));
        BindButton(typeof(Buttons));

        m_timeText = GetText((int)Texts.TimeText);

        Image iconImage = GetImage((int)Images.IconImage);
        iconImage.SetSprite(Enum.GetName(typeof(Define.Item), (Define.Item)m_type));

        Button buyButton = GetButton((int)Buttons.BuyButton);
        buyButton.gameObject.BindEvent(OnBuy);

        float width = CalcTextWidth(gameObject, 100.0f, iconImage.gameObject, buyButton.gameObject);
        GetText((int)Texts.NameText).SetWidth(width);
        GetText((int)Texts.DetailText).SetWidth(width);

        RefreshUI();

        return true;
    }

    private void Update()
    {
        if (m_timeText.gameObject.activeSelf == false)
            return;

        float remainTime = (float)Managers.Data.GetPotionSec(m_type);
        if (remainTime > 60.0f)
        {
            m_timeText.text = $"{(int)(remainTime / 60.0f)} {Managers.GetText(1000000020)}";
        }
        else
        {
            if (remainTime <= 0.0f)
                RefreshUI();
            else
                m_timeText.text = $"{(int)remainTime} {Managers.GetText(1000000021)}";
        }
    }

    public override void RefreshUI()
    {
        if (m_isInit == false)
            return;

        bool isPotion = Managers.Data.GetPotionSec(m_type) > 0.0f;

        GetText((int)Texts.PriceText).gameObject.SetActive(!isPotion);
        GetText((int)Texts.BuyText).gameObject.SetActive(!isPotion);
        m_timeText.gameObject.SetActive(isPotion);
        GetButton((int)Buttons.BuyButton).interactable = !isPotion;

        GetText((int)Texts.PriceText).text = $"{m_template.price.ToString("N0")}G";
        GetText((int)Texts.BuyText).text = Managers.GetText(1000000018);
        GetText((int)Texts.NameText).text = Managers.GetText(m_template.name);
        GetText((int)Texts.DetailText).text = Managers.GetText(m_template.desc);
    }

    void OnBuy()
    {
        Debug.Log("[UIShopItem:OnBuy] Click");

        if (Managers.Data.GetPotionSec(m_type) > 0.0f)
            return;

        if (Managers.Data.IsEnoughGold(m_template.price) == false)
            return;

        NetworkManager.Instance.RequestBuyItem(m_template.id);
        Managers.Sound.Play(Sound.Effect, "Buy");
    }
}
