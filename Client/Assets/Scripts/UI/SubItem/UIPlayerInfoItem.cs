using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlayerInfoItem : UIBase
{
    enum Texts
    {
        NameText,
        ValueText,
    }

    protected override bool Init()
    {
        if (base.Init() == false)
            return false;

        BindText(typeof(Texts));

        RefreshUI();

        return true;
    }

    public override void RefreshUI()
    {
        if (m_isInit == false)
            return;

        GetText((int)Texts.NameText).text = GetNameText();
        GetText((int)Texts.ValueText).text = GetValueText();
    }

    string GetNameText()
    {
        Define.PlayerInfo type = (Define.PlayerInfo)m_type;

        int id = 0;
        if (type == Define.PlayerInfo.Att)
            id = 1000000006;
        else if (type == Define.PlayerInfo.AttSpeed)
            id = 1000000007;
        else if (type == Define.PlayerInfo.CriRate)
            id = 1000000008;
        else if (type == Define.PlayerInfo.CriDamage)
            id = 1000000009;
        else if (type == Define.PlayerInfo.AttPotion)
            id = 1000000014;
        else if (type == Define.PlayerInfo.CriRatePotion)
            id = 1000000016;

        return Managers.GetText(id);
    }

    string GetValueText()
    {
        GameData data = Managers.Data.SaveData;
        Define.PlayerInfo type = (Define.PlayerInfo)m_type;

        string value = "";
        if (type == Define.PlayerInfo.Att)
        {
            value = GetValueText(Define.Stat.Att, data.attLv);
        }
        else if (type == Define.PlayerInfo.AttSpeed)
        {
            value = GetValueText(Define.Stat.AttSpeed, data.attSpeedLv);
        }
        else if (type == Define.PlayerInfo.CriRate)
        {
            value = GetValueText(Define.Stat.CriRate, data.criRateLv);
        }
        else if (type == Define.PlayerInfo.CriDamage)
        {
            value = GetValueText(Define.Stat.CriDamage, data.criDamageLv);
        }
        else if (type == Define.PlayerInfo.AttPotion)
        {
            if (Managers.Data.GetPotionSec(Define.Item.AttPotion) > 0.0f)
                value = Managers.Data.GetPotionDateLocalTime(Define.Item.AttPotion);
        }
        else if (type == Define.PlayerInfo.CriRatePotion)
        {
            if (Managers.Data.GetPotionSec(Define.Item.CriRatePotion) > 0.0f)
                value = Managers.Data.GetPotionDateLocalTime(Define.Item.CriRatePotion);
        }

        return value;
    }

    string GetValueText(Define.Stat statType, int level)
    {
        StatTemplate template = Managers.Template.GetStatTemplate(statType, level);
        return $"Lv.{level} {template.value}";
    }
}