using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Define;

public class UISetting : UIPopup
{
    enum Texts
    {
        TitleText,
        BgmText,
        EffectText,
        LanguageText,
        EngText,
        KorText,
    }

    enum Buttons
    {
        CloseButton,
    }

    enum Toggles
    {
        BgmToggle,
        EffectToggle,
        EngToggle,
        KorToggle,
    }

    protected override bool Init()
    {
        if (base.Init() == false)
            return false;

        BindText(typeof(Texts));
        BindButton(typeof(Buttons));
        BindToggle(typeof(Toggles));

        Button close = GetButton((int)Buttons.CloseButton);
        close.gameObject.BindEvent(OnClose);
        close.SetSprite("Close");

        Toggle bgm = GetToggle((int)Toggles.BgmToggle);
        Toggle effect = GetToggle((int)Toggles.EffectToggle);
        Toggle eng = GetToggle((int)Toggles.EngToggle);
        Toggle kor = GetToggle((int)Toggles.KorToggle);
        bgm.onValueChanged.AddListener(OnBgmToggle);
        effect.onValueChanged.AddListener(OnEffectToggle);
        eng.onValueChanged.AddListener(OnEngToggle);
        kor.onValueChanged.AddListener(OnKorToggle);

        RefreshUI();

        return true;
    }

    public override void RefreshUI()
    {
        if (m_isInit == false)
            return;

        GetText((int)Texts.TitleText).text = Managers.GetText(1000000024);
        GetText((int)Texts.BgmText).text = Managers.GetText(1000000025);
        GetText((int)Texts.EffectText).text = Managers.GetText(1000000026);
        GetText((int)Texts.LanguageText).text = Managers.GetText(1000000027);
        GetText((int)Texts.EngText).text = Managers.GetText(1000000028);
        GetText((int)Texts.KorText).text = Managers.GetText(1000000029);

        GameData data = Managers.Data.SaveData;
        bool isKor = data.language == Define.Language.Kor;
        GetToggle((int)Toggles.BgmToggle).isOn = data.isBgm;
        GetToggle((int)Toggles.EffectToggle).isOn = data.isEffect;
        GetToggle((int)Toggles.KorToggle).isOn = isKor;
        GetToggle((int)Toggles.EngToggle).isOn = !isKor;
    }

    void OnClose()
    {
        Debug.Log("[UISetting:OnClose] Click");

        Managers.UI.CloseUIPopup(this);
        Managers.Sound.Play(Sound.Effect, "Click");
    }

    void OnBgmToggle(bool isChange)
    {
        Debug.Log($"[UISetting:OnBgmToggle] {isChange}");

        Managers.Data.SetBgm(isChange);
    }

    void OnEffectToggle(bool isChange)
    {
        Debug.Log($"[UISetting:OnEffectToggle] {isChange}");

        Managers.Data.SetEffect(isChange);
    }

    void OnEngToggle(bool isChange)
    {
        Debug.Log($"[UISetting:OnEngToggle] {isChange}");

        if (isChange == false)
            return;

        Managers.Data.SetLanguage(Define.Language.Eng);
    }

    void OnKorToggle(bool isChange)
    {
        Debug.Log($"[UISetting:OnKorToggle] {isChange}");

        if (isChange == false)
            return;

        Managers.Data.SetLanguage(Define.Language.Kor);
    }
}
