using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Define;

public class UIPlayerInfo : UIPopup
{
    enum GameObjects
    {
        InfoPanel,
    }

    enum Texts
    {
        TitleText,
    }

    enum Buttons
    {
        CloseButton,
    }

    protected override bool Init()
    {
        if (base.Init() == false)
            return false;

        BindObject(typeof(GameObjects));
        BindText(typeof(Texts));
        BindButton(typeof(Buttons));

        GameObject infoPanel = GetObject((int)GameObjects.InfoPanel);
        Vector3 pos = infoPanel.transform.position;
        pos.y -= 85.0f;
        infoPanel.transform.position = pos;

        Button close = GetButton((int)Buttons.CloseButton);
        close.gameObject.BindEvent(OnClose);
        close.SetSprite("Close");

        m_subItems.Add(Managers.UI.MakePanelSubItem<UIPlayerInfoItem>(infoPanel, (int)Define.PlayerInfo.Max));

        RefreshUI();

        return true;
    }

    public override void RefreshUI()
    {
        if (m_isInit == false)
            return;

        GetText((int)Texts.TitleText).text = Managers.GetText(1000000023);

        RefreshUISubItems();
    }

    void OnClose()
    {
        Debug.Log("[UIPlayerInfo:OnClose] Click");

        Managers.UI.CloseUIPopup(this);
        Managers.Sound.Play(Sound.Effect, "Click");
    }
}
