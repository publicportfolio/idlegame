using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager
{
    public Dictionary<string, Sprite> m_sprites = new Dictionary<string, Sprite>();

    public T Load<T>(string path) where T : Object
    {
        if (typeof(T) == typeof(Sprite))
        {
            if (m_sprites.TryGetValue(path, out Sprite sp))
                return sp as T;

            Sprite sprite = Resources.Load<Sprite>(path);
            m_sprites.Add(path, sprite);
            return sprite as T;
        }

        return Resources.Load<T>(path);
    }

    public GameObject Instantiate(GameObject prefab, Transform parent = null)
    {
        GameObject go = Object.Instantiate(prefab, parent);
        go.name = prefab.name;
        return go;
    }

    public GameObject Instantiate(string path, Transform parent = null)
    {
        GameObject prefab = Load<GameObject>($"Prefabs/{path}");
        if (prefab == null)
        {
            Debug.Log($"[ResourceManager:Instantiate] Failed to load prefab : {path}");
            return null;
        }

        return Instantiate(prefab, parent);
    }

    public void Destroy(GameObject go)
    {
        if (go == null)
            return;

        Object.Destroy(go);
    }
}
