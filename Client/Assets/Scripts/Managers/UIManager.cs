using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Diagnostics;

public class UIManager
{
    int m_order = 10;

    UIScene m_uiScene;

    Stack<UIPopup> m_uiPopups = new Stack<UIPopup>();

    public GameObject Root
    {
        get
        {
            GameObject root = GameObject.Find("@UIRoot");
            if (root == null)
                root = new GameObject { name = "@UIRoot" };

            return root;
        }
    }

    public void Clear()
    {
        CloseAllUIPopup();
        m_uiScene = null;
    }

    public void SetCanvas(GameObject go, bool isSort)
    {
        Canvas canvas = Util.GetOrAddComponent<Canvas>(go);
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        canvas.overrideSorting = true;

        if (isSort)
        {
            canvas.sortingOrder = m_order;
            m_order++;
        }
        else
        {
            canvas.sortingOrder = 0;
        }
    }

    public List<UIBase> MakePanelSubItem<T>(GameObject panel, int maxCnt, bool isZero = true) where T : UIBase
    {
        List<UIBase> list = new List<UIBase>();

        int i = 0;
        if (isZero == false)
            i = 1;

        for (; i < maxCnt; i++)
        {
            T item = MakeSubItem<T>(panel.transform);
            item.Type = i;

            list.Add(item);
        }

        return list;
    }

    public T MakeSubItem<T>(Transform parent = null, string name = null) where T : UIBase
    {
        if (string.IsNullOrEmpty(name))
            name = typeof(T).Name;

        GameObject prefab = Managers.Resource.Load<GameObject>($"Prefabs/UI/SubItem/{name}");

        GameObject go = Managers.Resource.Instantiate(prefab);
        if (parent != null)
            go.transform.SetParent(parent);

        go.transform.localScale = Vector3.one;
        go.transform.localPosition = prefab.transform.position;

        return Util.GetOrAddComponent<T>(go);
    }

    public T ShowUIScene<T>(string name = null) where T : UIScene
    {
        if (string.IsNullOrEmpty(name))
            name = typeof(T).Name;

        GameObject go = Managers.Resource.Instantiate($"UI/Scene/{name}");
        T uiScene = Util.GetOrAddComponent<T>(go);
        m_uiScene = uiScene;

        go.transform.SetParent(Root.transform);

        return uiScene;
    }

    public T ShowUIPopup<T>(string name = null, Transform parent = null) where T : UIPopup
    {
        if (string.IsNullOrEmpty(name))
            name = typeof(T).Name;

        GameObject prefab = Managers.Resource.Load<GameObject>($"Prefabs/UI/Popup/{name}");

        GameObject go = Managers.Resource.Instantiate(prefab);
        T uiPopup = Util.GetOrAddComponent<T>(go);
        m_uiPopups.Push(uiPopup);

        if (parent != null)
            go.transform.SetParent(parent);
        else if (m_uiScene != null)
            go.transform.SetParent(m_uiScene.transform);
        else
            go.transform.SetParent(Root.transform);

        go.transform.localScale = Vector3.one;
        go.transform.localPosition = prefab.transform.position;

        return uiPopup;
    }

    public void RefreshUI()
    {
        if (m_uiScene != null)
            m_uiScene.RefreshUI();

        foreach (UIPopup uiPopup in m_uiPopups)
        {
            if (uiPopup != null)
                uiPopup.RefreshUI();
        }
    }

    public T FindUIScene<T>() where T : UIScene
    {
        if (m_uiScene == null)
            return null;

        if (m_uiScene.GetType() != typeof(T))
            return null;

        return m_uiScene as T;
    }

    public T FindUIPopup<T>() where T : UIPopup
    {
        return m_uiPopups.Where(x => x.GetType() == typeof(T)).FirstOrDefault() as T;
    }

    public T PeekUIPopup<T>() where T : UIPopup
    {
        if (m_uiPopups.Count == 0)
            return null;

        return m_uiPopups.Peek() as T;
    }

    public void CloseUIPopup()
    {
        if (m_uiPopups.Count == 0)
            return;

        UIPopup uiPopup = m_uiPopups.Pop();
        Managers.Resource.Destroy(uiPopup.gameObject);
        uiPopup = null;

        m_order--;
    }

    public void CloseUIPopup(UIPopup uiPopup)
    {
        if (m_uiPopups.Count == 0)
            return;

        if (m_uiPopups.Peek() != uiPopup)
        {
            Debug.Log("[UIManager:CloseUIPopup] Close Popup Failed!");
            return;
        }

        CloseUIPopup();
    }

    public void CloseAllUIPopup()
    {
        while (m_uiPopups.Count > 0)
            CloseUIPopup();
    }
}
