using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GameData
{
    public int gold;
    public int attLv;
    public int attSpeedLv;
    public int criRateLv;
    public int criDamageLv;
    public double attPotionDate;
    public double criRatePotionDate;

    public int stageType;
    public int stageKind;

    public bool isBgm;
    public bool isEffect;
    public Define.Language language;
}

public class DataManager
{
    GameData m_gameData = new GameData();
    public GameData SaveData { get { return m_gameData; } set { m_gameData = value; } }

    public bool IsEnoughGold(int price)
    {
        Debug.Log($"[DataManager:isEnoughGold] {m_gameData.gold >= price}");

        if (m_gameData.gold >= price)
            return true;

        return false;
    }

    public bool IsPlaySound(Define.Sound type)
    {
        bool isPlay = false;
        if (type == Define.Sound.Bgm)
            isPlay = m_gameData.isBgm;
        else if (type == Define.Sound.Effect)
            isPlay = m_gameData.isEffect;

        return isPlay;
    }

    public int GetLevel(Define.Stat type)
    {
        if (type == Define.Stat.Att)
            return m_gameData.attLv;
        else if (type == Define.Stat.AttSpeed)
            return m_gameData.attSpeedLv;
        else if (type == Define.Stat.CriRate)
            return m_gameData.criRateLv;
        else if (type == Define.Stat.CriDamage)
            return m_gameData.criDamageLv;

        return 0;
    }

    public int GetLevel(int type)
    {
        return GetLevel((Define.Stat)type);
    }

    public float GetValue(Define.Stat type)
    {
        float value = Managers.Template.GetStatTemplate(type, GetLevel(type)).value;

        if (type == Define.Stat.Att || type == Define.Stat.CriRate)
        {
            Define.Item itemType = Define.Item.None;

            if (type == Define.Stat.Att)
                itemType = Define.Item.AttPotion;
            else if (type == Define.Stat.CriRate)
                itemType = Define.Item.CriRatePotion;

            if (GetPotionSec(itemType) > 0.0f)
            {
                ItemTemplate template = Managers.Template.GetItemTemplate(itemType);
                value += value * template.value;
            }
        }

        return value;
    }

    public double GetPotionSec(Define.Item type)
    {
        DateTime originDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        double nowMilliSec = DateTime.Now.ToUniversalTime().Subtract(originDate).TotalMilliseconds;

        double milliSec = 0.0;
        if (type == Define.Item.AttPotion)
            milliSec = m_gameData.attPotionDate;
        else if (type == Define.Item.CriRatePotion)
            milliSec = m_gameData.criRatePotionDate;

        return (milliSec - nowMilliSec) / 1000.0;
    }

    public double GetPotionSec(int type)
    {
        return GetPotionSec((Define.Item)type);
    }

    public string GetPotionDateLocalTime(Define.Item type)
    {
        DateTime date = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        if (type == Define.Item.AttPotion)
            date = date.AddMilliseconds(m_gameData.attPotionDate);
        else if (type == Define.Item.CriRatePotion)
            date = date.AddMilliseconds(m_gameData.criRatePotionDate);

        return date.ToLocalTime().ToString("MM/dd HH:mm");
    }

    public void SetStage(int kind, int type = 0)
    {
        m_gameData.stageKind = kind;
        if (type != 0)
            m_gameData.stageType = type;
    }

    public void SetBgm(bool isPlay)
    {
        m_gameData.isBgm = isPlay;

        if (isPlay)
            Managers.Sound.Play(Define.Sound.Bgm, "Bgm");
        else
            Managers.Sound.Stop(Define.Sound.Bgm);
    }

    public void SetEffect(bool isPlay)
    {
        m_gameData.isEffect = isPlay;

        if (isPlay == false)
            Managers.Sound.Stop(Define.Sound.Effect);
    }

    public void SetLanguage(Define.Language lang)
    {
        m_gameData.language = lang;

        Managers.UI.RefreshUI();
    }
}
