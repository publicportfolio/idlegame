using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILoader<Key, Item>
{
    Dictionary<Key, Item> MakeDic();
}

public class TemplateManager
{
    public Dictionary<int, EnemyTemplate> Enemys { get; private set; } = new Dictionary<int, EnemyTemplate>();
    public Dictionary<int, ItemTemplate> Items { get; private set; } = new Dictionary<int, ItemTemplate>();
    public Dictionary<int, StageTemplate> Stages { get; private set; } = new Dictionary<int, StageTemplate>();
    public Dictionary<int, StatTemplate> Stats { get; private set; } = new Dictionary<int, StatTemplate>();
    public Dictionary<int, TextTemplate> Texts { get; private set; } = new Dictionary<int, TextTemplate>();

    public void Init()
    {
        Enemys = LoadJson<EnemyTemplateLoader, int, EnemyTemplate>("EnemyTemplate").MakeDic();
        Items = LoadJson<ItemTemplateLoader, int, ItemTemplate>("ItemTemplate").MakeDic();
        Stages = LoadJson<StageTemplateLoader, int, StageTemplate>("StageTemplate").MakeDic();
        Stats = LoadJson<StatTemplateLoader, int, StatTemplate>("StatTemplate").MakeDic();
        Texts = LoadJson<TextTemplateLoader, int, TextTemplate>("TextTemplate").MakeDic();
    }

    T LoadJson<T, Key, Value>(string path) where T : ILoader<Key, Value>
    {
        TextAsset textAsset = Managers.Resource.Load<TextAsset>($"Templates/{path}");
        return JsonUtility.FromJson<T>(textAsset.text);
    }

    public ItemTemplate GetItemTemplate(Define.Item type)
    {
        foreach (ItemTemplate template in Items.Values)
        {
            if ((int)type == template.type)
                return template;
        }

        return null;
    }

    public ItemTemplate GetItemTemplate(int type)
    {
        return GetItemTemplate((Define.Item)type);
    }

    public StageTemplate GetStageTemplate(int type, int kind)
    {
        foreach (StageTemplate template in Stages.Values)
        {
            if (template.type == type && template.kind == kind)
                return template;
        }

        return null;
    }

    public StatTemplate GetStatTemplate(Define.Stat type, int level)
    {
        foreach (StatTemplate template in Stats.Values)
        {
            if ((int)type + 1 == template.type && template.level == level)
                return template;
        }

        return null;
    }

    public StatTemplate GetStatTemplate(int type, int level)
    {
        return GetStatTemplate((Define.Stat)type, level);
    }

    public string GetText(int id)
    {
        if (Texts.TryGetValue(id, out TextTemplate template) == false)
            return "";

        if (Managers.Data.SaveData.language == Define.Language.Kor)
            return template.kor;

        return template.eng;
    }
}
