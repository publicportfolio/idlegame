using System.Collections;
using System.Collections.Generic;
using System.Resources;
using UnityEngine;
using UnityEngine.Diagnostics;
using UnityEngine.SceneManagement;

public class Managers : MonoBehaviour
{
    static Managers s_instance;
    static Managers Instance { get { Init(); return s_instance; } }

    DataManager m_dataManager = new DataManager();
    GameManager m_gameManager = new GameManager();
    ResourceManager m_resourceManager = new ResourceManager();
    SceneManagerEx m_sceneManager = new SceneManagerEx();
    SoundManager m_soundManager = new SoundManager();
    StageManager m_stageManager = new StageManager();
    TemplateManager m_templateManager = new TemplateManager();
    UIManager m_uiManager = new UIManager();

    public static DataManager Data { get { return Instance.m_dataManager; } }
    public static GameManager Game { get { return Instance.m_gameManager; } }
    public static ResourceManager Resource { get { return Instance.m_resourceManager; } }
    public static SceneManagerEx Scene { get { return Instance.m_sceneManager; } }
    public static SoundManager Sound { get { return Instance.m_soundManager; } }
    public static StageManager Stage { get { return Instance.m_stageManager; } }
    public static TemplateManager Template { get { return Instance.m_templateManager; } }
    public static UIManager UI { get { return Instance.m_uiManager; } }

    private void Start()
    {
        Init();
    }

    static void Init()
    {
        if (s_instance != null)
            return;

        GameObject go = GameObject.Find("@Managers");
        if (go == null)
            go = new GameObject { name = "@Managers" };

        s_instance = Util.GetOrAddComponent<Managers>(go);
        DontDestroyOnLoad(go);

        s_instance.m_templateManager.Init();
        s_instance.m_soundManager.Init();

        Application.targetFrameRate = 60;
    }

    private void Update()
    {
        Stage.Update();
    }

    public static string GetText(int id)
    {
        return Template.GetText(id);
    }
}
