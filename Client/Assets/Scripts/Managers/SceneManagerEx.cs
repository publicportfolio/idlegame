using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerEx
{
    public SceneBase CurScene { get { return GameObject.FindObjectOfType<SceneBase>(); } }

    public void LoadScene(Define.Scene type)
    {
        CurScene.Clear();

        SceneManager.LoadScene(System.Enum.GetName(typeof(Define.Scene), type));
    }
}
