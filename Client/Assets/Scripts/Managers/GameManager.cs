using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager
{
    PlayerController m_player;
    EnemyController m_enemy;
    Portal m_portal;

    public PlayerController Player { get { return m_player; } }
    public EnemyController Enemy { get { return m_enemy; } }
    public Portal PortalObject { get { return m_portal; } }

    public void Spawn(int id = 0)
    {
        if (id != 0)
        {
            EnemyTemplate template = Managers.Template.Enemys[id];
            m_enemy = Spawn<EnemyController>(template.name);
            m_enemy.Template = template;
        }
        else
        {
            m_player = Spawn<PlayerController>("HeroKnight");
        }
    }

    public void SpawnPortal()
    {
        m_portal = Spawn<Portal>("Portal");
    }

    T Spawn<T>(string name) where T : Component
    {
        return Util.GetOrAddComponent<T>(Managers.Resource.Instantiate(name));
    }

    public void Despawn(GameObject go)
    {
        if (m_player.gameObject == go)
            m_player = null;
        else if (m_enemy.gameObject == go)
            m_enemy = null;
        else if (m_portal.gameObject == go)
            m_portal = null;

        Managers.Resource.Destroy(go);
    }

    public void DespawnAll()
    {
        if (m_player != null)
            Managers.Resource.Destroy(m_player.gameObject);

        if (m_enemy != null)
            Managers.Resource.Destroy(m_enemy.gameObject);

        if (m_portal != null)
            Managers.Resource.Destroy(m_portal.gameObject);
    }
}
