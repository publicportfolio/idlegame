using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager
{
    private AudioSource[] m_audioSources = new AudioSource[(int)Define.Sound.Max];
    private Dictionary<string, AudioClip> m_audioClips = new Dictionary<string, AudioClip>();

    private GameObject m_root = null;

    public void Init()
    {
        if (m_root != null)
            return;

        m_root = GameObject.Find("@SoundRoot");
        if (m_root != null)
            return;

        m_root = new GameObject { name = "@SoundRoot" };
        Object.DontDestroyOnLoad(m_root);

        string[] typeNames = System.Enum.GetNames(typeof(Define.Sound));
        for (int cnt = 0; cnt < typeNames.Length - 1; cnt++)
        {
            GameObject go = new GameObject { name = typeNames[cnt] };
            m_audioSources[cnt] = go.AddComponent<AudioSource>();

            if (cnt == (int)Define.Sound.Bgm)
                m_audioSources[cnt].loop = true;

            go.transform.parent = m_root.transform;
        }
    }

    public void Clear()
    {
        foreach (AudioSource source in m_audioSources)
            source.Stop();

        m_audioClips.Clear();
    }

    public bool Play(Define.Sound type, string path, float volume = 1.0f)
    {
        if (string.IsNullOrEmpty(path))
            return false;

        if (Managers.Data.IsPlaySound(type) == false)
            return false;

        AudioSource source = m_audioSources[(int)type];
        if (type == Define.Sound.Bgm)
            source.volume = volume / 3.0f;
        else
            source.volume = volume;

        path = $"Sounds/{path}";

        if (type == Define.Sound.Bgm)
        {
            AudioClip clip = Managers.Resource.Load<AudioClip>(path);
            if (clip == null)
                return false;

            if (source.isPlaying)
                source.Stop();

            source.clip = clip;
            source.Play();

            return true;
        }
        else if (type == Define.Sound.Effect)
        {
            AudioClip clip = GetAudioClip(path);
            if (clip == null)
                return false;

            source.PlayOneShot(clip);

            return true;
        }

        return false;
    }

    public void Stop(Define.Sound type)
    {
        AudioSource audioSource = m_audioSources[(int)type];
        audioSource.Stop();
    }

    private AudioClip GetAudioClip(string path)
    {
        if (m_audioClips.TryGetValue(path, out AudioClip clip))
            return clip;

        clip = Managers.Resource.Load<AudioClip>(path);
        m_audioClips.Add(path, clip);

        return clip;
    }
}
