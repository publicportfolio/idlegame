using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Networking;
using static Define;

#region PacketData
[Serializable]
public class PacketData
{
    public PacketData(Define.Protocol value)
    {
        protocol = (int)value;
    }

    public int protocol;
    public string uuid = SystemInfo.deviceUniqueIdentifier;
}

[Serializable]
public class SavePacketData : PacketData
{
    public SavePacketData(Define.Protocol value) : base(value) { }

    public GameData data = Managers.Data.SaveData;
}

[Serializable]
public class IdPacketData : PacketData
{
    public IdPacketData(Define.Protocol value, int valueId) : base(value)
    {
        id = valueId;
    }

    public int id;
}
#endregion

public class NetworkManager : MonoBehaviour
{
    static NetworkManager s_instance;
    public static NetworkManager Instance { get { Init(); return s_instance; } }

    //private string m_url = "localhost";
    private string m_url = "ec2-3-35-11-78.ap-northeast-2.compute.amazonaws.com";
    private string m_port = "3000";

    void Awake()
    {
        Init();
    }

    static void Init()
    {
        if (s_instance != null)
            return;

        GameObject go = GameObject.Find("@NetworkManager");
        if (go == null)
            go = new GameObject { name = "@NetworkManager" };

        s_instance = Util.GetOrAddComponent<NetworkManager>(go);
        DontDestroyOnLoad(go);
    }

    public IEnumerator PostRequest(string jsonData, Action<string> callback)
    {
        Debug.Log($"[NetworkManager:PostRequest] {jsonData}");

        UnityWebRequest www = new UnityWebRequest($"http://{m_url}:{m_port}", "POST");
        byte[] jsonBytes = System.Text.Encoding.UTF8.GetBytes(jsonData);

        www.uploadHandler = new UploadHandlerRaw(jsonBytes);
        www.downloadHandler = new DownloadHandlerBuffer();
        www.SetRequestHeader("Content-Type", "application/json");

        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("POST request error: " + www.error);
            callback?.Invoke(null);
        }
        else
        {
            string response = www.downloadHandler.text;
            Debug.Log("[NetworkManager:PostRequest] Response: " + response);
            callback?.Invoke(response);
        }

        www.Dispose();
    }

    public void RequestLogin()
    {
        Debug.Log("[NetworkManager:requestLogin]");

        PacketData packet = new PacketData(Protocol.REQ_LOGIN);
        StartCoroutine(PostRequest(JsonUtility.ToJson(packet), ResponseLogin));
    }

    public void ResponseLogin(string data)
    {
        Debug.Log("[NetworkManager:responseLogin]");

        SavePacketData packet = JsonUtility.FromJson<SavePacketData>(data);
        if (packet.data.language == Define.Language.None)
        {
            if (Application.systemLanguage == SystemLanguage.Korean)
                packet.data.language = Define.Language.Kor;
            else
                packet.data.language = Define.Language.Eng;
        }

        Managers.Data.SaveData = packet.data;
        Managers.UI.RefreshUI();
    }

    public void RequestStageEnd()
    {
        Debug.Log("[NetworkManager:RequestStageEnd]");

        SavePacketData packet = new SavePacketData(Protocol.REQ_STAGE_END);
        StartCoroutine(PostRequest(JsonUtility.ToJson(packet), ResponseStageEnd));
    }

    public void ResponseStageEnd(string data)
    {
        Debug.Log("[NetworkManager:ResponseStageEnd]");

        Managers.Stage.RefreshStage();
    }

    public void RequestBuyStat(int id)
    {
        Debug.Log("[NetworkManager:requestBuyStat]");

        IdPacketData packet = new IdPacketData(Protocol.REQ_BUY_STAT, id);
        StartCoroutine(PostRequest(JsonUtility.ToJson(packet), ResponseBuyStat));
    }

    public void ResponseBuyStat(string data)
    {
        Debug.Log("[NetworkManager:responseBuyStat]");

        SavePacketData packet = JsonUtility.FromJson<SavePacketData>(data);
        Managers.Data.SaveData.gold = packet.data.gold;
        Managers.Data.SaveData.attLv = packet.data.attLv;
        Managers.Data.SaveData.attSpeedLv = packet.data.attSpeedLv;
        Managers.Data.SaveData.criRateLv = packet.data.criRateLv;
        Managers.Data.SaveData.criDamageLv = packet.data.criDamageLv;

        Managers.UI.RefreshUI();
    }

    public void RequestBuyItem(int id)
    {
        Debug.Log("[NetworkManager:requestBuyItem]");

        IdPacketData packet = new IdPacketData(Protocol.REQ_BUY_ITEM, id);
        StartCoroutine(PostRequest(JsonUtility.ToJson(packet), ResponseBuyItem));
    }

    public void ResponseBuyItem(string data)
    {
        Debug.Log("[NetworkManager:responseBuyItem]");

        SavePacketData packet = JsonUtility.FromJson<SavePacketData>(data);
        Managers.Data.SaveData.gold = packet.data.gold;

        Define.Item buyItemType = Define.Item.None;
        if (Managers.Data.SaveData.attPotionDate < packet.data.attPotionDate)
        {
            buyItemType = Define.Item.AttPotion;
            Managers.Data.SaveData.attPotionDate = packet.data.attPotionDate;
        }
        else if (Managers.Data.SaveData.criRatePotionDate < packet.data.criRatePotionDate)
        {
            buyItemType = Define.Item.CriRatePotion;
            Managers.Data.SaveData.criRatePotionDate = packet.data.criRatePotionDate;
        }

        Managers.UI.FindUIScene<UIGame>().MakePotionItem(buyItemType);
        Managers.UI.RefreshUI();
    }

    public void RequestEnemyDead(int id)
    {
        Debug.Log("[NetworkManager:RequestEnemyDead]");

        IdPacketData packet = new IdPacketData(Protocol.REQ_ENEMY_DEAD, id);
        StartCoroutine(PostRequest(JsonUtility.ToJson(packet), ResponseEnemyDead));
    }

    public void ResponseEnemyDead(string data)
    {
        Debug.Log("[NetworkManager:ResponseEnemyDead]");

        SavePacketData packet = JsonUtility.FromJson<SavePacketData>(data);
        Managers.Data.SaveData.gold = packet.data.gold;

        Managers.UI.RefreshUI();
    }
}
