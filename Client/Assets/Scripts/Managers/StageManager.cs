using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UIElements;

public class StageManager
{
    float m_playTime;
    GameObject[] m_background = new GameObject[3];

    public int Type { get { return Managers.Data.SaveData.stageType; } }
    public int Kind { get { return Managers.Data.SaveData.stageKind; } }
    public float PlayTime { get { return m_playTime; } }

    public void Init()
    {
        for (int i = 0; i < m_background.Length; i++)
        {
            GameObject go = Managers.Resource.Instantiate($"BG/Background{i + 1}");
            go.GetComponent<SpriteRenderer>().sortingOrder = -1;

            m_background[i] = go;
        }

        RefreshStage(true);
    }

    public void Update()
    {
        float time = m_playTime;
        if (time > 0.0f)
        {
            time -= Time.deltaTime;
            if (time <= 0.0f)
            {
                time = 0.0f;
                Managers.Game.Player.State = Define.State.Dead;
            }
            m_playTime = time;
        }
    }

    public void RefreshStage(bool isInit = false)
    {
        StageTemplate template = Managers.Template.GetStageTemplate(Type, Kind);
        m_playTime = template.time;

        for (int i = 0; i < m_background.Length; i++)
        {
            if (i == Type - 1)
                m_background[i].SetActive(true);
            else
                m_background[i].SetActive(false);
        }

        if (isInit == false)
            Managers.UI.RefreshUI();

        Managers.Game.DespawnAll();
        Managers.Game.Spawn();
        Managers.Game.Spawn(template.enemyId);
    }

    public void NextStage()
    {
        int type = Type;
        int kind = Kind;
        kind++;

        StageTemplate template = Managers.Template.GetStageTemplate(type, kind);
        if(template != null)
        {
            Managers.Data.SetStage(kind);
        }
        else
        {
            type++;
            kind = 1;
            template = Managers.Template.GetStageTemplate(type, kind);
            if (template != null)
                Managers.Data.SetStage(kind, type);
            else
                Managers.Data.SetStage(kind);
        }

        NetworkManager.Instance.RequestStageEnd();
    }

    public void ResetStage()
    {
        Managers.Data.SetStage(1);

        NetworkManager.Instance.RequestStageEnd();
    }

    public string GetStageName()
    {
        return Managers.GetText(1000000010 + Type - 1);
    }
}
