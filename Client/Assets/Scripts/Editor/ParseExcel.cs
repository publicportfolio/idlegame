using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ParseExcel : EditorWindow
{
    [MenuItem("Tools/ParseExcel")]
    public static void Parse()
    {
        ParseEnemyTemplate();
        ParseItemTemplate();
        ParseStageTemplate();
        ParseStatTemplate();
        ParseTextTemplate();
    }

    static void ParseEnemyTemplate()
    {
        EnemyTemplateLoader loader = new EnemyTemplateLoader();
        loader.enemyTemplates = new List<EnemyTemplate>();

        string[] lines = Resources.Load<TextAsset>("Templates/Excel/EnemyTemplate").text.Split("\n");
        for (int i = 1; i < lines.Length; i++)
        {
            string[] row = lines[i].Replace("\r", "").Split(',');
            if (row.Length == 0)
                continue;
            if (string.IsNullOrEmpty(row[0]))
                continue;

            EnemyTemplate template = new EnemyTemplate()
            {
                id = int.Parse(row[0]),
                type = int.Parse(row[1]),
                name = row[2],
                maxHp = float.Parse(row[3]),
                gold = int.Parse(row[4]),
                rate = float.Parse(row[5]),
            };

            loader.enemyTemplates.Add(template);
        }

        WriteJson(loader);
    }

    static void ParseItemTemplate()
    {
        ItemTemplateLoader loader = new ItemTemplateLoader();
        loader.itemTemplates = new List<ItemTemplate>();

        string[] lines = Resources.Load<TextAsset>("Templates/Excel/ItemTemplate").text.Split("\n");
        for (int i = 1; i < lines.Length; i++)
        {
            string[] row = lines[i].Replace("\r", "").Split(',');
            if (row.Length == 0)
                continue;
            if (string.IsNullOrEmpty(row[0]))
                continue;

            ItemTemplate template = new ItemTemplate()
            {
                id = int.Parse(row[0]),
                type = int.Parse(row[1]),
                name = int.Parse(row[2]),
                desc = int.Parse(row[3]),
                value = float.Parse(row[4]),
                time = int.Parse(row[5]),
                price = int.Parse(row[6]),
            };

            loader.itemTemplates.Add(template);
        }

        WriteJson(loader);
    }

    static void ParseStageTemplate()
    {
        StageTemplateLoader loader = new StageTemplateLoader();
        loader.stageTemplates = new List<StageTemplate>();

        string[] lines = Resources.Load<TextAsset>("Templates/Excel/StageTemplate").text.Split("\n");
        for (int i = 1; i < lines.Length; i++)
        {
            string[] row = lines[i].Replace("\r", "").Split(',');
            if (row.Length == 0)
                continue;
            if (string.IsNullOrEmpty(row[0]))
                continue;

            StageTemplate template = new StageTemplate()
            {
                id = int.Parse(row[0]),
                type = int.Parse(row[1]),
                kind = int.Parse(row[2]),
                time = float.Parse(row[3]),
                enemyId = int.Parse(row[4]),
            };

            loader.stageTemplates.Add(template);
        }

        WriteJson(loader);
    }

    static void ParseStatTemplate()
    {
        StatTemplateLoader loader = new StatTemplateLoader();
        loader.statTemplates = new List<StatTemplate>();

        string[] lines = Resources.Load<TextAsset>("Templates/Excel/StatTemplate").text.Split("\n");
        for (int i = 1; i < lines.Length; i++)
        {
            string[] row = lines[i].Replace("\r", "").Split(',');
            if (row.Length == 0)
                continue;
            if (string.IsNullOrEmpty(row[0]))
                continue;

            StatTemplate template = new StatTemplate()
            {
                id = int.Parse(row[0]),
                type = int.Parse(row[1]),
                level = int.Parse(row[2]),
                value = float.Parse(row[3]),
                price = int.Parse(row[4]),
            };

            loader.statTemplates.Add(template);
        }

        WriteJson(loader);
    }

    static void ParseTextTemplate()
    {
        TextTemplateLoader loader = new TextTemplateLoader();
        loader.textTemplates = new List<TextTemplate>();

        string[] lines = Resources.Load<TextAsset>("Templates/Excel/TextTemplate").text.Split("\n");
        for (int i = 1; i < lines.Length; i++)
        {
            string[] row = lines[i].Replace("\r", "").Split(',');
            if (row.Length == 0)
                continue;
            if (string.IsNullOrEmpty(row[0]))
                continue;

            TextTemplate template = new TextTemplate()
            {
                id = int.Parse(row[0]),
                eng = row[1],
                kor = row[2],
            };

            loader.textTemplates.Add(template);
        }

        WriteJson(loader);
    }

    static void WriteJson<T>(T loader)
    {
        string name = typeof(T).Name;
        name = name.Substring(0, name.IndexOf("Loader"));

        File.WriteAllText($"{Application.dataPath}/Resources/Templates/{name}.json", JsonUtility.ToJson(loader, true));
        AssetDatabase.Refresh();

        Debug.Log($"[ParseExcel:WriteJson] {name} parse success!");
    }
}
