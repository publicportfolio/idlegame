using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Define;

public class BaseController : MonoBehaviour
{
    private Define.State m_state = Define.State.Idle;

    protected Animator m_anim;

    public Define.State State
    {
        get { return m_state; }
        set
        {
            if (m_anim == null)
                return;

            m_state = value;
            string name = Enum.GetName(typeof(Define.State), m_state);
            m_anim.Play(name);

            if (IsAttackState())
            {
                m_anim.speed = Managers.Data.GetValue(Define.Stat.AttSpeed);
            }
            else
            {
                m_anim.speed = 1.0f;
            }
        }
    }

    private void Start()
    {
        Init();
    }

    protected virtual void Init()
    {
        m_anim = GetComponent<Animator>();
    }

    private void Update()
    {
        switch (m_state)
        {
            case Define.State.Idle:
            UpdateIdle();
            break;
            case Define.State.Move:
            UpdateMove();
            break;
            case Define.State.Hurt:
            UpdateHurt();
            break;
            case Define.State.Dead:
            UpdateDead();
            break;
            case Define.State.Attack1:
            case Define.State.Attack2:
            case Define.State.Attack3:
            UpdateAttack();
            break;
        }
    }

    protected virtual void UpdateIdle()
    {

    }

    protected virtual void UpdateMove()
    {

    }

    protected virtual void UpdateHurt()
    {

    }

    protected virtual void UpdateDead()
    {

    }

    protected virtual void UpdateAttack()
    {

    }

    protected bool IsEndAni(Define.State state)
    {
        string aniName = Enum.GetName(typeof(Define.State), state);

        if (m_anim.GetCurrentAnimatorStateInfo(0).IsName(aniName) &&
            m_anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
            return true;

        return false;
    }

    protected bool IsAttackState()
    {
        if (m_state == Define.State.Attack1 ||
            m_state == Define.State.Attack2 ||
            m_state == Define.State.Attack3)
            return true;

        return false;
    }
}
