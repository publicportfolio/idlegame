using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class EnemyController : BaseController
{
    EnemyTemplate m_template;
    bool m_isHurt = false;
    float m_maxHp;
    float m_hp;

    public EnemyTemplate Template { set { m_template = value; } }
    public float MaxHp { get { return m_maxHp; } }
    public float Hp { get { return m_hp; } }

    protected override void Init()
    {
        base.Init();

        m_maxHp = m_template.maxHp * GetHpRate();
        m_hp = m_maxHp;
    }

    protected override void UpdateHurt()
    {
        if (IsEndAni(Define.State.Hurt))
        {
            m_isHurt = false;
            State = Define.State.Idle;
        }
    }

    protected override void UpdateDead()
    {
        if (IsEndAni(Define.State.Dead))
        {
            Managers.Game.SpawnPortal();
            Managers.Game.Despawn(gameObject);
        }
    }

    public void OnAttacked()
    {
        if (m_isHurt || State == Define.State.Dead)
            return;

        float damage = Managers.Data.GetValue(Define.Stat.Att);

        if (Managers.Data.GetValue(Define.Stat.CriRate) >= Random.Range(1.0f, 100.0f))
            damage *= Managers.Data.GetValue(Define.Stat.CriDamage);

        m_hp -= damage;

        if (m_hp <= 0.0f)
        {
            m_hp = 0.0f;

            State = Define.State.Dead;
            NetworkManager.Instance.RequestEnemyDead(m_template.id);
        }
        else
        {
            m_isHurt = true;
            State = Define.State.Hurt;
            Managers.Sound.Play(Define.Sound.Effect, "Hurt");
        }
    }

    float GetHpRate()
    {
        float rate = 1.0f;
        int stageKind = Managers.Stage.Kind;

        Define.Enemy type = (Define.Enemy)m_template.type;
        if (type == Define.Enemy.Normal)
        {
            rate += m_template.rate * ((stageKind / 1) - 1);
        }
        else if (type == Define.Enemy.MiddleBoss)
        {
            rate += m_template.rate * ((stageKind / 10) - 1);
        }
        else if (type == Define.Enemy.Boss)
        {
            rate += m_template.rate * ((stageKind / 50) - 1);
        }

        return rate;
    }
}
