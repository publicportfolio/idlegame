using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Define;

public class PlayerController : BaseController
{
    bool m_isHit = false;
    float m_moveSpeed = 1.0f;
    float m_attRange = 1.0f;

    GameObject m_target;

    protected override void UpdateIdle()
    {
        if (m_target != null)
        {
            State = Define.State.Move;
        }
        else
        {
            if (Managers.Game.Enemy != null && Managers.Game.Enemy.State != State.Dead)
            {
                m_target = Managers.Game.Enemy.gameObject;
                return;
            }

            if (Managers.Game.PortalObject != null)
            {
                m_target = Managers.Game.PortalObject.gameObject;
                return;
            }
        }
    }

    protected override void UpdateMove()
    {
        Vector3 dir = m_target.transform.position - transform.position;
        dir.y = 0.0f;

        transform.position += dir.normalized * m_moveSpeed * Time.deltaTime;

        if (Managers.Game.Enemy != null && Managers.Game.Enemy.gameObject == m_target)
        {
            if (dir.magnitude <= m_attRange)
                State = Define.State.Attack1;
        }
    }

    protected override void UpdateDead()
    {
        if (IsEndAni(Define.State.Dead))
        {
            Managers.Game.Despawn(gameObject);
            Managers.Stage.ResetStage();
        }
    }

    protected override void UpdateAttack()
    {
        EnemyController enemy = m_target.GetComponent<EnemyController>();
        if (enemy.State == Define.State.Dead)
        {
            m_isHit = false;
            m_target = null;
            State = Define.State.Idle;
            return;
        }
        else
        {
            if (m_isHit)
                enemy.OnAttacked();
        }

        if (IsEndAni(Define.State.Attack1))
            State = Define.State.Attack2;
        else if (IsEndAni(Define.State.Attack2))
            State = Define.State.Attack3;
        else if (IsEndAni(Define.State.Attack3))
            State = Define.State.Idle;
    }

    void OnHitEvent()
    {
        if (IsAttackState() == false)
            return;

        m_isHit = true;
    }

    void OnHitEndEvent()
    {
        if (IsAttackState() == false)
            return;

        m_isHit = false;
    }
}
