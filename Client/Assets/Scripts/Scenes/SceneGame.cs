using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneGame : SceneBase
{
    protected override bool Init()
    {
        if (base.Init() == false)
            return false;

        m_sceneType = Define.Scene.Game;

        Managers.Stage.Init();
        Managers.UI.ShowUIScene<UIGame>();
        Managers.Sound.Play(Define.Sound.Bgm, "Bgm");

        Debug.Log("[SceneGame:Init]");

        return true;
    }
}
