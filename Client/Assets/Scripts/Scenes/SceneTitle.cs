using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTitle : SceneBase
{
    protected override bool Init()
    {
        if (base.Init() == false)
            return false;

        m_sceneType = Define.Scene.Title;

        Managers.UI.ShowUIScene<UITitle>();
        NetworkManager.Instance.RequestLogin();

        Debug.Log("[SceneTitle:Init]");

        return true;
    }
}
