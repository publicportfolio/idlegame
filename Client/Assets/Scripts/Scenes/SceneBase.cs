using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneBase : MonoBehaviour
{
    protected Define.Scene m_sceneType = Define.Scene.None;

    protected bool m_isInit = false;

    public Define.Scene SceneType { get { return m_sceneType; } }

    void Start()
    {
        Init();
    }

    protected virtual bool Init()
    {
        if (m_isInit)
            return false;

        GameObject go = GameObject.Find("EventSystem");
        if (go == null)
            Managers.Resource.Instantiate("UI/EventSystem").name = "@EventSystem";

        return m_isInit = true;
    }

    public virtual void Clear()
    {

    }
}
