using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TextTemplate
{
    public int id;
    public string eng;
    public string kor;
}

[Serializable]
public class TextTemplateLoader : ILoader<int, TextTemplate>
{
    public List<TextTemplate> textTemplates = new List<TextTemplate>();

    public Dictionary<int, TextTemplate> MakeDic()
    {
        Dictionary<int, TextTemplate> dic = new Dictionary<int, TextTemplate>();

        foreach (TextTemplate template in textTemplates)
            dic.Add(template.id, template);

        return dic;
    }
}
