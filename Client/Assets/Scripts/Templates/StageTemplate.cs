using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class StageTemplate
{
    public int id;
    public int type;
    public int kind;
    public float time;
    public int enemyId;
}

[Serializable]
public class StageTemplateLoader : ILoader<int, StageTemplate>
{
    public List<StageTemplate> stageTemplates = new List<StageTemplate>();

    public Dictionary<int, StageTemplate> MakeDic()
    {
        Dictionary<int, StageTemplate> dic = new Dictionary<int, StageTemplate>();

        foreach (StageTemplate template in stageTemplates)
            dic.Add(template.id, template);

        return dic;
    }
}