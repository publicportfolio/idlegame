using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ItemTemplate
{
    public int id;
    public int type;
    public int name;
    public int desc;
    public float value;
    public int time;
    public int price;
}

[Serializable]
public class ItemTemplateLoader : ILoader<int, ItemTemplate>
{
    public List<ItemTemplate> itemTemplates = new List<ItemTemplate>();

    public Dictionary<int, ItemTemplate> MakeDic()
    {
        Dictionary<int, ItemTemplate> dic = new Dictionary<int, ItemTemplate>();

        foreach (ItemTemplate template in itemTemplates)
            dic.Add(template.id, template);

        return dic;
    }
}