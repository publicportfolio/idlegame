using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class StatTemplate
{
    public int id;
    public int type;
    public int level;
    public float value;
    public int price;
}

[Serializable]
public class StatTemplateLoader : ILoader<int, StatTemplate>
{
    public List<StatTemplate> statTemplates = new List<StatTemplate>();

    public Dictionary<int, StatTemplate> MakeDic()
    {
        Dictionary<int, StatTemplate> dic = new Dictionary<int, StatTemplate>();

        foreach (StatTemplate template in statTemplates)
            dic.Add(template.id, template);

        return dic;
    }
}