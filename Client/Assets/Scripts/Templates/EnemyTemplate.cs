using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EnemyTemplate
{
    public int id;
    public int type;
    public string name;
    public float maxHp;
    public int gold;
    public float rate;
}

[Serializable]
public class EnemyTemplateLoader : ILoader<int, EnemyTemplate>
{
    public List<EnemyTemplate> enemyTemplates = new List<EnemyTemplate>();

    public Dictionary<int, EnemyTemplate> MakeDic()
    {
        Dictionary<int, EnemyTemplate> dic = new Dictionary<int, EnemyTemplate>();

        foreach (EnemyTemplate template in enemyTemplates)
            dic.Add(template.id, template);

        return dic;
    }
}